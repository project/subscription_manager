<?php

/**
 * @file
 * Contains subscription_plan.page.inc.
 *
 * Page callback for Subscription plan entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Subscription plan templates.
 *
 * Default template: subscription_plan.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_subscription_plan(array &$variables) {
  // Fetch SubscriptionPlanEntity Entity Object.
  $subscription_plan = $variables['elements']['#subscription_plan'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
