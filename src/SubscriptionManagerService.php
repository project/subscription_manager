<?php

namespace Drupal\subscription_manager;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\subscription_manager\Entity\SubscriptionEntity;
use Drupal\subscription_manager\Entity\SubscriptionPlanEntity;
use Drupal\user\Entity\User;

/**
 * Class SubscriptionManagerService.
 *
 * @package Drupal\subscription_manager
 */
class SubscriptionManagerService {

  use MessengerTrait;

  /**
   * Drupal\Core\Config\ConfigFactory definition.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * @var \Drupal\subscription_manager\SubscriptionManagerConnectorManager
   */
  private $subscriptionManagerConnectorManager;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   * @param \Drupal\subscription_manager\SubscriptionManagerConnectorManager $subscription_manager_connector_manager
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, LoggerChannelInterface $logger, SubscriptionManagerConnectorManager $subscription_manager_connector_manager) {
    $this->config = $config_factory->get('subscription_manager.settings');
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger;
    $this->subscriptionManagerConnectorManager = $subscription_manager_connector_manager;
  }

  /**
   * Check if a given user has a Subscription on any plan.
   *
   * @param \Drupal\user\UserInterface|\Drupal\Core\Session\AccountInterface|\Drupal\Core\Entity\EntityInterface $user
   *   The user.
   * @param null $connector_plugin_id
   *
   * @return bool
   *   TRUE if the user has a subscription.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function userHasLocalSubscriptionToAnyPlan($user, $connector_plugin_id = NULL): bool {
    return (bool) $this->getUserLocalSubscriptionToAnyPlan($user, $connector_plugin_id);
  }

  /**
   *
   */
  public function shouldRedirectCurrentUserOnLogin(): bool {
    if ($this->config->get('redirect')) {
      $request = \Drupal::service('request_stack')->getCurrentRequest();
      if (!$request->request->has('destination')) {
        $user = User::load(\Drupal::currentUser()->id());
        if (!$this->userHasLocalSubscriptionToAnyPlan($user)) {
          $connector_plugin_id = $this->config->get('default_connector');
          /** @var \Drupal\subscription_manager\SubscriptionManagerConnectorInterface $connector */
          $connector = $this->subscriptionManagerConnectorManager->createInstance($connector_plugin_id);
          return $connector->shouldRedirectCurrentUserOnLogin($user);
        }
      }
    }
    return FALSE;
  }

  /**
   * @param \Drupal\user\UserInterface|\Drupal\Core\Session\AccountInterface|UserInterface $user
   *   The user.
   * @param null $connector_plugin_id
   *
   * @return bool|\Drupal\subscription_manager\Entity\SubscriptionEntity
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getUserLocalSubscriptionToAnyPlan($user, $connector_plugin_id = NULL) {
    $params = [
      'user_id' => $user->id(),
    ];
    if ($connector_plugin_id) {
      $params['connector_plugin_id'] = $connector_plugin_id;
    }
    return $this->loadLocalSubscription($params);
  }

  /**
   * Determine is user has a subscription for a specific plan.
   *
   * @param \Drupal\user\Entity\UserInterface|\Drupal\Core\Entity\EntityInterface $user
   * @param \Drupal\subscription_manager\Entity\SubscriptionPlanEntity $plan
   *
   * @return bool
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function userIsSubscribedToPlan($user, SubscriptionPlanEntity $plan): bool {
    $params = [
      'user_id' => $user->id(),
      'plan_id' => $plan->getPlanId(),
      'status' => 1,
    ];
    return (bool) $this->loadLocalSubscription($params);
  }

  /**
   * @param \Drupal\user\Entity\User|\Drupal\Core\Entity\EntityInterface $user
   * @param null $connector_plugin_id
   *
   * @return bool|string
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getLocalUserCustomerIdFromSubscription($user, $connector_plugin_id = NULL) {
    if ($subscription = $this->getUserLocalSubscriptionToAnyPlan($user, $connector_plugin_id)) {
      return $subscription->getCustomerId();
    }

    return FALSE;
  }

  /**
   * Load a local subscription.
   *
   * @param array $properties
   *   Local properties by which to filter the subscriptions.
   *
   * @return \Drupal\subscription_manager\Entity\SubscriptionEntity|bool
   *   A Subscription entity, or else FALSE.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function loadLocalSubscription($properties = []) {
    $subscription_entities = $this->loadLocalSubscriptionMultiple($properties);
    if (!count($subscription_entities)) {
      return FALSE;
    }

    $first = reset($subscription_entities);

    return $first;
  }

  /**
   * Load multiple local subscriptions.
   *
   * @param array $properties
   *   Local properties by which to filter the subscriptions.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   An array of Subscription entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function loadLocalSubscriptionMultiple($properties = []) {
    $subscription_entities = $this->entityTypeManager
      ->getStorage('subscription')
      ->loadByProperties($properties);

    return $subscription_entities;
  }

  /**
   * @return NULL|SubscriptionEntity[]
   */
  public function loadExpiredSubscriptions(): ?array {
    $query = \Drupal::entityQuery('subscription');
    $query->condition('status', TRUE)
      ->condition('expiry', \Drupal::time()->getRequestTime(), '<=')
      ->accessCheck(FALSE);
    $subscriptions = [];
    if ($ids = $query->execute()) {
      $subscriptions = SubscriptionEntity::loadMultiple($ids);
    }
    return $subscriptions;
  }

  /**
   * @param array $properties
   *
   * @return bool|\Drupal\Core\Entity\EntityInterface|mixed
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function loadLocalPlan($properties = []) {
    $subscription_plan_entities = $this->entityTypeManager
      ->getStorage('subscription_plan')
      ->loadByProperties($properties);

    if (!count($subscription_plan_entities)) {
      return FALSE;
    }

    $first = reset($subscription_plan_entities);

    return $first;
  }

  /**
   * Load multiple local plans.
   *
   * @param array $properties
   *
   * @return \Drupal\subscription_manager\Entity\SubscriptionPlanEntity[]
   *   An array of entity objects indexed by their IDs. Returns an empty array
   *   if no matching entities are found.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function loadLocalPlanMultiple($properties = []): array {
    $query = $this->entityTypeManager
      ->getStorage('subscription_plan')
      ->getQuery()
      ->accessCheck(FALSE)
      ->sort('weight');

    // @see buildPropertyQuery().
    if ($properties) {
      foreach ($properties as $name => $value) {
        $query->condition($name, (array) $value, 'IN');
      }
    }

    $entity_ids = $query->execute();
    return $this->entityTypeManager
      ->getStorage('subscription_plan')
      ->loadMultiple($entity_ids);
  }

  /**
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function syncPlans(): void {
    $plugin_definitions = $this->subscriptionManagerConnectorManager->getDefinitions();
    foreach ($plugin_definitions as $plugin_definition) {
      /** @var \Drupal\subscription_manager\SubscriptionManagerConnectorInterface $connector */
      $connector = $this->subscriptionManagerConnectorManager->createInstance($plugin_definition['id']);
      $connector->syncPlans();
    }
  }

  /**
   * @return \Drupal\subscription_manager\SubscriptionManagerConnectorManager
   */
  public function getConnectorManager(): SubscriptionManagerConnectorManager {
    return $this->subscriptionManagerConnectorManager;
  }

  public function getDefaultConnector() {
    return $this->config->get('default_connector');
  }

}
