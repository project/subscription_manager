<?php

namespace Drupal\subscription_manager;

use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\user\UserInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 *
 */
interface SubscriptionManagerConnectorInterface {

  /**
   *
   */
  public function createLocalSubscription($remote_subscription);

  /**
   * Loads a user's remote subscription.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user.
   *
   * @return bool|array|\stdClass
   */
  public function loadRemoteSubscriptionsByUser($user);

  /**
   *
   */
  public function loadRemoteSubscriptionMultiple();

  /**
   *
   */
  public function loadRemotePlanById($plan_id);

  /**
   *
   */
  public function loadRemotePlanMultiple($args = []);

  /**
   *
   */
  public function syncPlans();

  /**
   *
   */
  public function syncRemoteSubscriptionToLocal($remote_subscription);

  /**
   *
   */
  public function redirectToPortal($user): TrustedRedirectResponse;

  /**
   *
   */
  public function redirectToSubscribe($user): RedirectResponse;

  /**
   *
   */
  public function shouldRedirectCurrentUserOnLogin(UserInterface $user): bool;

}
