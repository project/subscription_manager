<?php

namespace Drupal\subscription_manager;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the Subscription entity.
 *
 * @see \Drupal\subscription_manager\Entity\SubscriptionEntity.
 */
class SubscriptionEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\subscription_manager\Entity\SubscriptionEntityInterface $entity */
    switch ($operation) {
      case 'view':
      case 'delete':
      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'administer subscriptions');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::forbidden();
  }

}
