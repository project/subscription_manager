<?php

namespace Drupal\subscription_manager\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\UserInterface;

/**
 * Defines the Subscription plan entity.
 *
 * @ingroup subscription_manager
 *
 * @ContentEntityType(
 *   id = "subscription_plan",
 *   label = @Translation("Subscription plan"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\subscription_manager\SubscriptionPlanEntityListBuilder",
 *     "views_data" = "Drupal\subscription_manager\Entity\SubscriptionPlanEntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\subscription_manager\Form\SubscriptionPlanEntityForm",
 *       "add" = "Drupal\subscription_manager\Form\SubscriptionPlanEntityForm",
 *       "edit" = "Drupal\subscription_manager\Form\SubscriptionPlanEntityForm",
 *       "delete" = "Drupal\subscription_manager\Form\SubscriptionPlanEntityDeleteForm",
 *     },
 *     "access" = "Drupal\subscription_manager\SubscriptionPlanEntityAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\subscription_manager\SubscriptionPlanEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "subscription_plan",
 *   admin_permission = "administer subscription plan entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/subscription-plan/{subscription_plan}",
 *     "add-form" = "/subscription-plan/add",
 *     "edit-form" = "/subscription-plan/{subscription_plan}/edit",
 *     "delete-form" = "/subscription-plan/{subscription_plan}/delete",
 *     "collection" = "/admin/content/subscription-plan",
 *   },
 *   field_ui_base_route = "subscription_plan.settings"
 * )
 */
class SubscriptionPlanEntity extends ContentEntityBase implements SubscriptionPlanEntityInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   *
   */
  public function getRoles() {
    return $this->get('roles');
  }

  /**
   *
   */
  public function getConnectorPluginId() {
    return $this->get('connector_plugin_id')->value;
  }

  /**
   *
   */
  public function getPlanId() {
    return $this->get('plan_id')->value;
  }

  /**
   *
   */
  public function getStatus() {
    return $this->get('status')->value;
  }

  /**
   *
   */
  public function getWeight() {
    return $this->get('weight')->value;
  }

  /**
   *
   */
  public function getData() {
    return $this->get('data');
  }

  /**
   *
   */
  public function getPricesData() {
    return $this->get('prices');
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the New Subscription plan entity entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['plan_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Subscription plan ID'))
      ->setDescription(t('The Stripe ID for this plan.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setCardinality(1)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ]);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Subscription plan entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setCardinality(1)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ]);

    $fields['connector_plugin_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Connector plugin ID'))
      ->setDescription(t('The ID of the Subscription Manager plugin that manages this plan.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setCardinality(1)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ]);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Enabled'))
      ->setDescription(t('If this plan is enabled or disabled.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
        'on_label' => new TranslatableMarkup('Enabled'),
      ])
      ->setDefaultValue('')
      ->setCardinality(1)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -4,
      ]);

    $fields['show'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Show on subscribe blocks'))
      ->setDescription(t('Display on subscribe blocks.'))
      ->setSettings([
        'on_label' => new TranslatableMarkup('Shown'),
      ])
      ->setDefaultValue('')
      ->setCardinality(1)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -4,
      ]);

    $fields['weight'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Weight'))
      ->setDescription(t('Weight of the subscription plan.'))
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'number_integer',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 20,
      ]);

    $fields['data'] = BaseFieldDefinition::create('map')
      ->setLabel(t('Plan data'))
      ->setDescription(t('Array of raw plan data.'));

    $fields['prices'] = BaseFieldDefinition::create('map')
      ->setLabel(t('Prices data'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDescription(t('Array of raw subscription plan prices data.'));

    $roles = user_roles(TRUE);
    $role_options = [];
    foreach ($roles as $rid => $role) {
      if (!$role->isAdmin()) {
        $role_options[$rid] = $role->label();
      }
    }
    // @todo Prevent administrator roles from being added here.
    $fields['roles'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Roles'))
      ->setDescription(t('Roles that will be granted to users actively subscribed to this plan. Warning: these roles will be removed from users who have cancelled or unpaid subscriptions for this plan!'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
        'allowed_values' => $role_options,
      ])
      ->setCardinality(-1)
      ->setRequired(FALSE)
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
        'size' => 10,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -4,
        'size' => 10,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
