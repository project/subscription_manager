<?php

namespace Drupal\subscription_manager\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Subscription plan entities.
 */
class SubscriptionPlanEntityViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['subscription_plan']['table']['base'] = [
      'field' => 'id',
      'title' => $this->t('Subscription plan'),
      'help' => $this->t('The Subscription plan ID.'),
    ];

    return $data;
  }

}
