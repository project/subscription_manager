<?php

namespace Drupal\subscription_manager\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\UserInterface;

/**
 * Defines the Subscription entity.
 *
 * @ingroup subscription_manager
 *
 * @ContentEntityType(
 *   id = "subscription",
 *   label = @Translation("Subscription"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\subscription_manager\SubscriptionEntityListBuilder",
 *     "views_data" = "Drupal\subscription_manager\Entity\SubscriptionEntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\subscription_manager\Form\SubscriptionEntityForm",
 *       "add" = "Drupal\subscription_manager\Form\SubscriptionEntityForm",
 *       "edit" = "Drupal\subscription_manager\Form\SubscriptionEntityForm",
 *       "delete" = "Drupal\subscription_manager\Form\SubscriptionEntityDeleteForm",
 *     },
 *     "access" = "Drupal\subscription_manager\SubscriptionEntityAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\subscription_manager\SubscriptionEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "subscription",
 *   admin_permission = "administer Subscription entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status"
 *   },
 *   links = {
 *     "canonical" = "/subscription/{subscription}",
 *     "add-form" = "/subscription/add",
 *     "edit-form" = "/subscription/{subscription}/edit",
 *     "delete-form" = "/subscription/{subscription}/delete",
 *     "collection" = "/admin/content/subscription",
 *   },
 *   field_ui_base_route = "subscription.settings"
 * )
 */
class SubscriptionEntity extends ContentEntityBase implements SubscriptionEntityInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    if ($account->isAnonymous()) {
      throw new \InvalidArgumentException('Anonymous users cannot be set as the owner of a subscription.');
    }

    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getConnectorPluginId() {
    return $this->get('connector_plugin_id')->value;
  }

  /**
   * @retun Drupal\subscription_manager\Entity\SubscriptionPlanEntity
   */
  public function getPlan() {
    $plans = $this->entityTypeManager()
      ->getStorage('subscription_plan')
      ->loadByProperties([
        'plan_id' => $this->getPlanId(),
      ]);

    if ($plans) {
      return reset($plans);
    }

    return NULL;
  }

  /**
   *
   */
  public function getPlanId() {
    return $this->get('plan_id')->value;
  }

  /**
   *
   */
  public function getCustomerId() {
    return $this->get('customer_id')->value;
  }

  /**
   *
   */
  public function getSubscriptionId() {
    return $this->get('subscription_id')->value;
  }

  /**
   *
   */
  public function getStatus() {
    return $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Owner'))
      ->setDescription(t('The owner of this subscription.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setRequired(TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Subscription entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ]);

    $fields['subscription_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Subscription ID'))
      ->setDescription(t('The remote ID for this subscription.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setRequired(TRUE);

    $fields['plan_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Plan ID'))
      ->setDescription(t('The remote ID for this plan.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setRequired(TRUE);

    $fields['customer_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Customer ID'))
      ->setDescription(t("The remote ID for this subscription's customer."))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setRequired(TRUE);

    $fields['connector_plugin_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Plugin ID'))
      ->setDescription(t("The machine name of the plugin that manages the remote subscription connection."))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setRequired(TRUE);

    $fields['expiry'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Expiry'))
      ->setDescription(t('The license expiration date and time.'))
      ->setDisplayOptions('form', array(
        'type' => 'datetime_timestamp',
        'weight' => 4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Active'))
      ->setDescription(t('Whether the subscription is active or inactive.'))
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['data'] = BaseFieldDefinition::create('map')
      ->setLabel(t('Subscription data'))
      ->setDescription(t('Array of raw subscription data.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    // If the subscription's plan is being updated, remove the old roles.
    // New roles will be added in postSave().
    if ($this->id() && $original = $storage->loadUnchanged($this->id())) {
      if ($original->getPlanId() && $original->getPlanId() != $this->getPlanId()) {
        $this->removeRoles($original->getPlan()->getRoles()->getIterator());
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage);
    $this->updateUserRoles();
  }

  /**
   * {@inheritdoc}
   */
  public static function preDelete(EntityStorageInterface $storage, array $entities) {
    /** @var SubscriptionEntity $entity */
    foreach ($entities as $entity) {
      $entity->updateUserRoles();
    }
  }

  /**
   * Update user roles.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function updateUserRoles(): void {
    if (!$this->getOwner()) {
      \Drupal::logger('subscription_manager')->error('Could not find user for subscription @sub', [
        '@sub' => $this->id(),
      ]);

      return;
    }

    if ($this->getOwner()->isAnonymous()) {
      \Drupal::logger('subscription_manager')->error('User for subscription @sub is anonymous, cannot add role.', [
        '@sub' => $this->id(),
      ]);

      return;
    }
    // There's a bug here. If a plan is deleted, then an expiring subscription will fail to remove the
    // Corresponding user roles. We should probably store the roles on the subscriber object and use that to
    // remove roles when the subscription is deleted.
    $plan = $this->getPlan();
    if ($plan) {
      $roles = $plan->getRoles()->getIterator();

      // Add roles.
      if ($this->getStatus()) {
        $this->addRoles($roles);
      }
      // Remove roles.
      else {
        \Drupal::logger('subscription_manager')
          ->info('Removing roles from user @user for subscription #@sub because status is @status ...', [
            '@user' => $this->getOwner()->label(),
            '@sub' => $this->id(),
            '@status' => $this->getStatus(),
          ]);
        $this->removeRoles($roles);
      }
    }
    else {
      \Drupal::logger('subscription_manager')->error('Could not find local plan matching remote plan id @plan_id for subscription @sub and user @user', [
        '@plan_id' => $this->getPlanId(),
        '@sub' => $this->id(),
        '@user' => $this->getOwner()->label(),
      ]);
    }
  }

  /**
   * @param $roles
   *
   * @return void
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function removeRoles($roles): void {
    foreach ($roles as $role) {
      $rid = $role->value;
      if ($this->getOwner()->hasRole($rid)) {
        $this->getOwner()->removeRole($rid);
        $this->getOwner()->save();
        \Drupal::logger('subscription_manager')
          ->info('Removing role @rid from user @user for subscription #@sub', [
            '@rid' => $rid,
            '@user' => $this->getOwner()->label(),
            '@sub' => $this->id(),
            '@status' => $this->getStatus(),
          ]);
      }
    }
  }

  /**
   * @param $roles
   *
   * @return void
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function addRoles($roles): void {
    foreach ($roles as $role) {
      $rid = $role->value;
      if (!$this->getOwner()->hasRole($rid)) {
        $this->getOwner()->addRole($rid);
        $this->getOwner()->save();
        \Drupal::logger('subscription_manager')
          ->info('Adding role @rid to user @user for subscription #@sub because its status is @status', [
            '@rid' => $rid,
            '@user' => $this->getOwner()
              ->label() . '(uid:' . $this->getOwnerId() . ')',
            '@sub' => $this->id(),
            '@status' => $this->getStatus(),
          ]);
      }
    }
  }

}
