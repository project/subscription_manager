<?php

namespace Drupal\subscription_manager\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Subscription plan entities.
 *
 * @ingroup subscription_manager
 */
interface SubscriptionPlanEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Subscription plan name.
   *
   * @return string
   *   Name of the Subscription plan.
   */
  public function getName();

  /**
   * Sets the Subscription plan name.
   *
   * @param string $name
   *   The Subscription plan name.
   *
   * @return \Drupal\subscription_manager\Entity\SubscriptionPlanEntityInterface
   *   The called Subscription plan entity.
   */
  public function setName($name);

  /**
   * Gets the Subscription creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Subscription.
   */
  public function getCreatedTime();

  /**
   * Sets the Subscription creation timestamp.
   *
   * @param int $timestamp
   *   The Subscription creation timestamp.
   *
   * @return \Drupal\subscription_manager\Entity\SubscriptionEntityInterface
   *   The called Subscription entity.
   */
  public function setCreatedTime($timestamp);

}
