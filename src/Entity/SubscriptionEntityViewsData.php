<?php

namespace Drupal\subscription_manager\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Subscription entities.
 */
class SubscriptionEntityViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['subscription']['table']['base'] = [
      'field' => 'id',
      'title' => $this->t('Subscription'),
      'help' => $this->t('The subscription ID.'),
    ];

    return $data;
  }

}
