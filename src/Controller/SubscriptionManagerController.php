<?php

namespace Drupal\subscription_manager\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\subscription_manager\SubscriptionManagerService;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class UserSubscriptionsController.
 *
 * @package Drupal\subscription_manager\Controller
 */
class SubscriptionManagerController extends ControllerBase {

  /**
   * @var \Drupal\subscription_manager\SubscriptionManagerService
   */
  protected $subscriptionManager;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  private $logger;

  /**
   * UserSubscriptionsController constructor.
   *
   * @param \Drupal\subscription_manager\SubscriptionManagerService $subscription_manager
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   */
  public function __construct(SubscriptionManagerService $subscription_manager, LoggerChannelInterface $logger, AccountProxyInterface $current_user) {
    $this->subscriptionManager = $subscription_manager;
    $this->logger = $logger;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('subscription_manager'),
      $container->get('logger.channel.subscription_manager'),
      $container->get('current_user'),
    );
  }

  /**
   * Redirect.
   */
  public function redirectToManageSubscription(): RedirectResponse {
    return $this->redirect('subscription_manager.manage_subscription', ['user' => $this->currentUser()->id()]);
  }

  /**
   * @param \Drupal\Core\Session\AccountInterface $account
   *
   * @return \Drupal\Core\Access\AccessResult
   */
  public function redirectToManageSubscriptionAccess(AccountInterface $account): AccessResult {
    return $this->manageSubscriptionAccess($account);
  }

  /**
   * @param \Drupal\Core\Session\AccountInterface $account
   *
   * @return \Drupal\Core\Access\AccessResult
   */
  public function manageSubscriptionAccess(AccountInterface $account): AccessResult {
    if ($account->hasPermission('administer subscriptions')) {
      if ($this->subscriptionManager->userHasLocalSubscriptionToAnyPlan($account)) {
        return AccessResult::allowed()->cachePerPermissions()->cachePerUser();
      }
    }

    if ($account->hasPermission('manage own subscriptions')) {
      if ($this->subscriptionManager->userHasLocalSubscriptionToAnyPlan($account)) {
        return AccessResult::allowed()->cachePerPermissions()->cachePerUser();
      }
    }

    return AccessResult::forbidden()->cachePerPermissions()->cachePerUser();
  }

  /**
   * Redirect the user to a page where they can manage their subscription.
   *
   * @param int $user
   *   The user ID.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The redirect response or a render array.
   */
  public function manageSubscription($user) {
    // @todo Remove this after recharge migration!
    $config = \Drupal::config('gg_subscription_manager.settings');
    $disable_manage_subscription = $config->get('disable_manage_subscription');
    if ($disable_manage_subscription) {
      return [
        '#markup' => '<h2>Temporarily disabled for maintentance</h2>' .
          '<p>The ability to manage subscriptions is currently disabled due to scheduled maintenance. '
      ];
    }

    /** @var \Drupal\user\Entity\UserInterface $user_object */
    $user_object = User::load($user);

    // It shouldn't be necessary to do this, but I cannot for the life of
    // me figure out why the _custom_access method is not called for this route.
    $access_result = $this->manageSubscriptionAccess($user_object);
    if ($access_result->isForbidden()) {
      throw new NotFoundHttpException();
    }

    $subscription = $this->subscriptionManager->getUserLocalSubscriptionToAnyPlan($user_object);
    // As per manageSubscriptionAccess(), users should not be able to access
    // this page if the $user has no subscription. But, user 0 could access.
    if (!$subscription) {
      $this->logger->error('User %user attempted to access the manage subscription page, but has no subscription.', [
        '%user' => $user_object->id(),
      ]);
      $this->messenger()->addMessage('You do not have a subscription.', Messenger::TYPE_ERROR);
      return $this->redirect('subscription_manager.subscribe');
    }
    $connector_plugin_id = $subscription->getConnectorPluginId();
    /** @var \Drupal\subscription_manager\SubscriptionManagerConnectorInterface $connector */
    $connector = $this->subscriptionManager->getConnectorManager()->createInstance($connector_plugin_id);
    return $connector->redirectToPortal($user);
  }

  /**
   * @return string
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function subscribeTitle(): string {
    if ($this->subscriptionManager->userHasLocalSubscriptionToAnyPlan($this->currentUser())) {
      return 'Upgrade';
    }
    return 'Subscribe';
  }

  /**
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function subscribe() {
    $user = User::load($this->currentUser()->id());
    // If user already has a subscription, send them to the corresponding connector's subscribe page.
    if ($subscription = $this->subscriptionManager->getUserLocalSubscriptionToAnyPlan($user)) {
      $connector_plugin_id = $subscription->getConnectorPluginId();
    }
    else {
      $connector_plugin_id = $this->config('subscription_manager.settings')->get('default_connector');
    }

    /** @var \Drupal\subscription_manager\SubscriptionManagerConnectorInterface $connector */
    $connector = $this->subscriptionManager->getConnectorManager()->createInstance($connector_plugin_id);
    return $connector->redirectToSubscribe($user);
  }

  /**
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return \Symfony\Component\HttpFoundation\Response
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \JsonException
   */
  public function getMySubscription(Request $request): Response {
    $subscription = $this->subscriptionManager->getUserLocalSubscriptionToAnyPlan(User::load($this->currentUser()->id()));
    if ($subscription) {
      $data = [
        'subscription' => $subscription->toArray(),
        'plan' => $subscription->getPlan()->toArray(),
      ];
    }
    else {
      $data = [
        'subscription' => FALSE,
      ];
    }

    return new Response(json_encode($data, JSON_THROW_ON_ERROR), Response::HTTP_ACCEPTED);
  }

}
