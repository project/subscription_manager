<?php

namespace Drupal\subscription_manager;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class to build a listing of Subscription entities.
 *
 * @ingroup subscription_manager
 */
class SubscriptionEntityListBuilder extends EntityListBuilder {


  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * @var \Drupal\subscription_manager\SubscriptionManagerService
   */
  private $subscriptionManager;

  /**
   * Constructs a new UserListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\subscription_manager\SubscriptionManagerService $subscription_manager
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, DateFormatterInterface $date_formatter, SubscriptionManagerService $subscription_manager) {
    parent::__construct($entity_type, $storage);
    $this->dateFormatter = $date_formatter;
    $this->subscriptionManager = $subscription_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('date.formatter'),
      $container->get('subscription_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Drupal Sub ID');
    $header['user_id'] = $this->t('Drupal User ID');
    $header['plan_id'] = $this->t('Remote Plan ID');
    $header['subscription_id'] = $this->t('Remote Subs ID');
    $header['customer_id'] = $this->t('Remote User ID');
    $header['status'] = $this->t('Status');
    $header['connector_plugin_id'] = $this->t('Connector');
    $header['created'] = $this->t('Created');
    $header['changed'] = $this->t('Changed');
    return $header + parent::buildHeader();
  }

  /**
   * @param \Drupal\subscription_manager\Entity\SubscriptionEntity $entity
   */
  public function buildRow(EntityInterface $entity) {
    $row['id'] = Link::fromTextAndUrl(
      $entity->id(),
      new Url(
        'entity.subscription.canonical', [
          'subscription' => $entity->id(),
        ]
      )
    );
    $row['user_id'] = Link::fromTextAndUrl(
      $entity->getOwnerId(),
      new Url(
        'entity.user.canonical', [
          'user' => $entity->getOwnerId(),
        ]
      )
    );
    $row['plan_id'] = Link::fromTextAndUrl(
      $entity->getPlanId(),
      new Url(
        'entity.subscription_plan.canonical', [
          'subscription_plan' => $this->subscriptionManager->loadLocalPlan(['plan_id' => $entity->getPlanId()])->id(),
        ]
      )
    );
    $row['subscription_id'] = $entity->getSubscriptionId();
    $row['customer_id'] = $entity->getCustomerId();
    $row['status'] = $entity->getStatus();
    $row['connector_plugin_id'] = $entity->getConnectorPluginId();
    $row['created'] = $this->dateFormatter->format($entity->getCreatedTime());
    $row['changed'] = $this->dateFormatter->format($entity->getChangedTime());

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritDoc}
   */
  protected function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);

    return $operations;
  }

}
