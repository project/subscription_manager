<?php

namespace Drupal\subscription_manager;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the Subscription plan entity.
 *
 * @see \Drupal\subscription_manager\Entity\SubscriptionPlanEntity.
 */
class SubscriptionPlanEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\subscription_manager\Entity\SubscriptionPlanEntityInterface $entity */
    switch ($operation) {
      case 'view':
      case 'delete':
      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'administer subscription plans');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::forbidden();
  }

}
