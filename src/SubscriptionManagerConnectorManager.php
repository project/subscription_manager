<?php

namespace Drupal\subscription_manager;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\subscription_manager\Annotation\SubscriptionManagerConnector;

/**
 * Provides a SubscriptionManagerConnector plugin manager.
 *
 * @see \Drupal\Core\Archiver\Annotation\Archiver
 * @see \Drupal\Core\Archiver\ArchiverInterface
 * @see plugin_api
 */
class SubscriptionManagerConnectorManager extends DefaultPluginManager {

  /**
   * Constructs a SubscriptionManagerConnectorManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/SubscriptionManagerConnector',
      $namespaces,
      $module_handler,
      SubscriptionManagerConnectorInterface::class,
      SubscriptionManagerConnector::class
    );
    $this->alterInfo('subscription_manager_connector_info');
    $this->setCacheBackend($cache_backend, 'subscription_manager_connector_info_plugins');
  }

}
