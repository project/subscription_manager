<?php

namespace Drupal\subscription_manager\Plugin\Menu;

use Drupal\Core\Menu\MenuLinkDefault;

/**
 *
 */
class SubscribeMenuLink extends MenuLinkDefault {

  /**
   * {@inheritdoc}
   *
   * @see \Drupal\subscription_manager\Controller\UserSubscriptionsController::subscribeTitle()
   */
  public function getTitle(): string {
    /** @var \Drupal\subscription_manager\SubscriptionManagerService $subscription_manager */
    $subscription_manager = \Drupal::service('subscription_manager');
    $current_user = \Drupal::service('current_user');
    if ($subscription_manager->userHasLocalSubscriptionToAnyPlan($current_user)) {
      return 'Upgrade';
    }
    return 'Subscribe';
  }

  /**
   *
   */
  public function getCacheContexts() {
    return ['user'];
  }

}
