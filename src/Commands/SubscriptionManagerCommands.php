<?php

namespace Drupal\subscription_manager\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\subscription_manager\SubscriptionManagerService;
use Drupal\user\RoleInterface;
use Drush\Commands\DrushCommands;

/**
 * SubscriptionManagerCommands class.
 */
class SubscriptionManagerCommands extends DrushCommands {

  /**
   * @var \Drupal\subscription_manager\SubscriptionManagerService
   */
  private $subscriptionManager;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * SubscriptionManagerCommands constructor.
   *
   * @param \Drupal\subscription_manager\SubscriptionManagerService $subscription_manager
   */
  public function __construct(SubscriptionManagerService $subscription_manager, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct();
    $this->subscriptionManager = $subscription_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Sync subscription plans from all connectors.
   *
   * @usage subscription_manager:sync-plans
   *   Usage description
   *
   * @command subscription_manager:sync-plans
   * @aliases foo
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function SyncPlans() {
    $this->subscriptionManager->syncPlans();
  }

  /**
   * Get a list of subscription plans.
   *
   * @field-labels
   *   plan_id: Plan ID
   *   name: Name
   *   connector_plugin_id: Connector ID
   *   status: Status
   *   roles: Roles
   * @default-fields plan_id,name,connector_plugin_id,roles,status
   *
   * @command subscription_manager:list-plans
   *
   * @filter-default-field name
   *
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields
   */
  public function ListPlans($options = ['format' => 'table']) {
    $plans = $this->subscriptionManager->loadLocalPlanMultiple();

    $rows = [];
    foreach ($plans as $plan) {
      $row = [
        'plan_id' => $plan->getPlanId(),
        'connector_plugin_id' => $plan->getConnectorPluginId(),
        'name' => $plan->getName(),
        'status' => $plan->getStatus(),
      ];

      $roles = user_role_names(TRUE);
      unset($roles[RoleInterface::AUTHENTICATED_ID]);
      $plan_roles = [];
      foreach ($plan->getRoles() as $role) {
        $role_value = $role->getValue()['value'];
        if (isset($roles[$role_value])) {
          $plan_roles[] = $roles[$role_value];
        }
      }
      asort($plan_roles);
      $row['roles'] = implode(',', $plan_roles);

      $rows[] = $row;
    }

    return new RowsOfFields($rows);
  }

  /**
   * Get a list of connectors.
   *
   * @field-labels
   *   label: Label
   *   connector_plugin_id: Connector ID
   *   provider: Provider Module
   * @default-fields label,connector_plugin_id,provider
   *
   * @command subscription_manager:list-connectors
   *
   * @filter-default-field label
   *
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields
   */
  public function ListConnectors($options = ['format' => 'table']) {
    $definitions = $this->subscriptionManager->getConnectorManager()->getDefinitions();
    foreach ($definitions as $connector_plugin_id => $definition) {
      $rows[] = [
        'provider' => $definition['provider'],
        'connector_plugin_id' => $connector_plugin_id,
        'label' => $definition['label'],
      ];
    }
    return new RowsOfFields($rows);
  }

}
