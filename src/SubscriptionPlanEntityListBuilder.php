<?php

namespace Drupal\subscription_manager;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Subscription plan entities.
 *
 * @ingroup subscription_manager
 */
class SubscriptionPlanEntityListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['plan_id'] = $this->t('Subscription plan ID');
    $header['name'] = $this->t('Name');
    $header['connector_plugin_id'] = $this->t('Connector');
    $header['status'] = $this->t('Status');
    $header['show'] = $this->t('Show');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\subscription_manager\Entity\SubscriptionPlanEntity $entity */
    $row['plan_id'] = $entity->plan_id->value;
    $row['name'] = $entity->label();
    $row['connector_plugin_id'] = $entity->getConnectorPluginId();
    $row['status'] = $entity->get('status')->value;
    $row['show'] = $entity->get('show')->value;

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   *
   * Builds the entity listing as renderable array for table.html.twig.
   */
  public function render() {
    $build = parent::render();
    $build['table']['#footer'] = [
      'data' => [
        [
          'data' => $this->t(
            'Visit the %link page to synchronize with plans from connectors.',
            [
              '%link' => Link::createFromRoute('Subscription manager configuration', 'subscription_manager.admin')->toString(),
            ]
          ),
          'colspan' => count($build['table']['#header']),
        ],
      ],
    ];
    return $build;
  }

}
