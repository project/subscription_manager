<?php

namespace Drupal\subscription_manager\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\subscription_manager\SubscriptionManagerService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SubscriptionManagerAdminForm.
 *
 * Contains admin form functionality for the Subscription manager.
 */
class SubscriptionManagerAdminForm extends ConfigFormBase {

  /**
   * @var \Drupal\subscription_manager\SubscriptionManagerConnectorManager
   */
  private $subscriptionManager;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\subscription_manager\SubscriptionManagerService $subscription_manager
   */
  public function __construct(ConfigFactoryInterface $config_factory, SubscriptionManagerService $subscription_manager) {
    $this->subscriptionManager = $subscription_manager;

    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('subscription_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'subscription_manager_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'subscription_manager.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('subscription_manager.settings');

    $plugin_definitions = $this->subscriptionManager->getConnectorManager()->getDefinitions();
    $connectors = array_keys($plugin_definitions);

    $form['default_connector'] = [
      '#type' => 'select',
      '#title' => t('Default subscription manager connector'),
      '#options' => array_combine($connectors, $connectors),
      '#description' => t('The default connector will be used for creating new subscriptions.'),
      '#default_value' => $config->get('default_connector'),
    ];

    $form['redirect'] = [
      '#type' => 'checkbox',
      '#title' => t('Redirect users to Subscription page after login'),
      '#description' => t('If checked, users will immediately be redirected to the Subscription page after login if 1) They do not already have a subscription, and 2) The request does not already have a redirect query parameter.'),
      '#default_value' => $config->get('redirect'),
    ];

    $form['actions']['sync-plans'] = [
      '#type' => 'submit',
      '#value' => t('Sync plans from connectors'),
      '#description' => t('This will create new plans and removed plans that no longer exist upstream.'),
      '#submit' => [[$this, 'syncPlansSubmit']],
      '#weight' => 100,
    ];

    $form['custom_subscribe_url'] = [
      '#type' => 'textfield',
      '#title' => t('Custom Subscribe URL'),
      '#description' => t('Override the default subscribe for the default connector'),
      '#default_value' => $config->get('custom_subscribe_url'),
    ];
    $form['token_help'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => ['user'],
      '#dialog' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('subscription_manager.settings')
      ->set('default_connector', $form_state->getValue('default_connector'))
      ->set('redirect', $form_state->getValue('redirect'))
      ->set('custom_subscribe_url', $form_state->getValue('custom_subscribe_url'))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Sync plans for all connectors.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function syncPlansSubmit() {
    $this->subscriptionManager->syncPlans();
  }

}
