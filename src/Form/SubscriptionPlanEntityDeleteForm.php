<?php

namespace Drupal\subscription_manager\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Subscription plan entities.
 *
 * @ingroup subscription_manager
 */
class SubscriptionPlanEntityDeleteForm extends ContentEntityDeleteForm {

}
